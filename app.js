//Obtém seleção do botão do usuário
const seletor = document.querySelectorAll("button");

//set values for cara and coroa
let cara = 1;
let coroa = 0;
let userScore = 0;
let computerScore = 0;
//Seleciona a pontuação do DOM
const pontuacaoJogadorExibida = document.getElementById("player-score");
const pontuacaoCPUExibida = document.getElementById("computer-score");

function displaySelections(user, computer) {
  const escolhaJogador = document.getElementById("player-selection");
  const escolhaCPU = document.getElementById("computer-selection");
  if (user === "cara") {
    escolhaJogador.style.color = "green";
  } else {
    escolhaJogador.style.color = "blue";
  }

  if (computer === "cara") {
    escolhaCPU.style.color = "green";
  } else {
    escolhaCPU.style.color = "blue";
  }
  escolhaJogador.innerHTML = `${user}`;
  escolhaCPU.innerHTML = `${computer}`;
}

function displayRandom(random) {
  const resultadoMoeda = document.getElementById("image");
  console.log(random);

  if (random === 1) {
    resultadoMoeda.style.backgroundImage = "url('./cara.png')";
  } else {
    resultadoMoeda.style.backgroundImage = "url('./coroa.png')";
  }
}

function tallyScore(random, userPick, computerPick) {
  const vencedor = document.getElementById("vencedor");

  if (userPick === random) {
    userScore++;
  }
  if (computerPick === random) {
    computerScore++;
  }

  pontuacaoJogadorExibida.textContent = `${userScore}`;
  pontuacaoCPUExibida.textContent = `${computerScore}`;

  if (userScore === 5 && computerScore === 5) {
    vencedor.innerHTML = `<h1>É um empate!</h1>`;
  } else if (userScore === 5) {
    vencedor.innerHTML = `<h1>Você ganhou!!!</h1>`;
  } else if (computerScore === 5) {
    vencedor.innerHTML = `<h1>O computador ganhou!!!</h1>`;
  }
}

function verificaAnterior() {
  if (
    pontuacaoJogadorExibida.textContent >= 5 ||
    pontuacaoCPUExibida.textContent >= 5
  ) {
    pontuacaoJogadorExibida.textContent = 0;
    pontuacaoCPUExibida.textContent = 0;
    userScore = 0;
    computerScore = 0;
    vencedor.innerHTML = null;
  }
}

// Adiciona um listener nos botões
seletor.forEach(function (button) {
  button.addEventListener("click", function (e) {
    verificaAnterior();

    const random = Math.round(Math.random());
    //Guarda seleção do usuário
    const escolhaDoJogador = e.target.id;
    let userPick;

    if (escolhaDoJogador === "cara") {
      userPick = 1;
      var escolhaDoCPU = "coroa";
      var computerPick = 0;
    } else if (escolhaDoJogador === "coroa") {
      userPick = 0;
      var escolhaDoCPU = "cara";
      var computerPick = 1;
    }

    //Gira a moeda
    const girar = document.getElementById("image");
    girar.classList.add("animate");

    //Exibe a escolha do jogador na parte desejada do DOM
    displaySelections(escolhaDoJogador, escolhaDoCPU);
    displayRandom(random);

    //Adiciona a pontuação para os jogadores
    setTimeout(function () {
      tallyScore(random, userPick, computerPick);
      //Reseta animações
      document.getElementById("image").classList.remove("animate");
    }, 2000);
  });
});
